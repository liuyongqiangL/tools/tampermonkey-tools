const newRegExp = (pattern: string) => new RegExp(pattern.replace(/\*/g, '.*'));

const matchUrl = (
  url: string,
  matchPatterns: string[],
  includePatterns: string[],
  excludePatterns: string[],
) => {
  const includeRegexes = includePatterns.map((pattern) => newRegExp(pattern));
  const excludeRegexes = excludePatterns.map((pattern) => newRegExp(pattern));

  return includeRegexes.some((regex) => {
    if (!regex.test(url)) return false;

    return (
      excludeRegexes.every((excludeRegex) => !excludeRegex.test(url)) &&
      (matchPatterns.length === 0 ||
        matchPatterns.some((matchPattern) => newRegExp(matchPattern).test(url)))
    );
  });
};

const currentUrl = window.location.href;

if (typeof userDatas !== 'undefined') {
  userDatas.forEach(({ matchRules, includeRules, excludeRules, runAt, fileContent }) => {
    if (matchUrl(currentUrl, matchRules, includeRules, excludeRules)) {
      try {
        const callbackfn = () => eval(fileContent);
        switch (runAt) {
          case 'document-start':
            callbackfn();
            break;
          case 'document-body':
            const observer = new MutationObserver((mutations) => {
              mutations.some((mutation) => {
                const addedNodes = Array.from(mutation.addedNodes);
                const hasBody = addedNodes.some((node) => node.nodeName.toLowerCase() === 'body');
                if (hasBody) {
                  observer.disconnect();
                  callbackfn();
                  return true;
                }
                return false;
              });
            });

            observer.observe(document.documentElement, { childList: true, subtree: true });
            break;
          case 'document-end':
            document.addEventListener('DOMContentLoaded', callbackfn);
            break;
          case 'document-idle':
            requestIdleCallback(callbackfn);
            break;
          default:
            break;
        }
      } catch (error) {}
    }
  });
}
