type UserDatas = {
  runAt: string;
  matchRules: string[];
  includeRules: string[];
  excludeRules: string[];
  fileContent: string;
}[];

declare var userDatas: UserDatas;
