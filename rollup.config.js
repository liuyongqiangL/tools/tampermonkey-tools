// rollup.config.js
import { defineConfig } from 'rollup';
import userScriptHeader from 'rollup-plugin-tampermonkey-header';
import path from 'path';
import fs from 'fs';
import glob from 'glob';
import pkg from './package.json';

const isDev = process.env.BUILD === 'development';

const userDefinedOptions = {
  metaPath: path.resolve(__dirname, 'src', 'meta.json'),
};

const isObject = (arg) => {
  return Object.prototype.toString.call(arg) === '[object Object]';
};
const mergeRollupConfigs = function (object, ...sources) {
  sources.forEach((source) => {
    if (!isObject(source)) {
      return;
    }
    Object.entries(source).forEach(([name, value]) => {
      if (name in object) {
        if (value === null || value === undefined) {
          return;
        }
        const objectValue = object[name];
        if (Array.isArray(objectValue)) {
          if (name === 'plugins') {
            const pluginNames = objectValue.map((plugin) => plugin.name);
            value.forEach((plugin) => {
              const index = pluginNames.indexOf(plugin.name);
              if (index > -1) {
                Object.assign(objectValue[index], plugin);
              } else {
                objectValue.push(plugin);
              }
            });
          } else {
            object[name] = Array.from(
              new Set([
                ...objectValue,
                ...(typeof value === 'object' ? Object.values(value) : [value]),
              ]),
            );
          }
        } else if (isObject(objectValue)) {
          Object.assign(object[name], value);
        } else {
          object[name] = value;
        }
      } else {
        object[name] = value;
      }
    });
  });
  return object;
};

const commonConfigs = require('./rollup_configs/default.js');
const rollupConfigsPath = require('path').join(__dirname, 'rollup_configs');
try {
  const files = fs.readdirSync(rollupConfigsPath);
  files.forEach(function (file) {
    if (file === 'default.js') return;
    const configs = require('./rollup_configs/' + file).default;
    mergeRollupConfigs(commonConfigs, configs);
  });
} catch (err) {
  console.log(err);
}

class CustomTampermonkeyPlugin {
  constructor() {
    this.userScriptHeaderContent = [];
    this.userDataMap = new Map();
    this.userGrantsMap = new Map();
  }

  getMetaPath = (dirPath) => {
    const filePath = path.join(dirPath, 'meta.json');
    const stats = fs.existsSync(filePath) && fs.statSync(filePath);
    return stats && stats.isFile() ? filePath : userDefinedOptions.metaPath;
  };

  getScriptConfig = (targetDir) => {
    const scriptFiles = glob.sync('src/scripts/**/index.{ts,js}');
    const mainFileName = 'main';

    return scriptFiles.concat(glob.sync(`src/${mainFileName}.ts`)).map((file) => {
      const relativePath = path.relative(
        './',
        file.slice(0, file.length - path.extname(file).length),
      );
      const isMain = relativePath === path.join('src', mainFileName);

      const relativeToSrc = path.relative('./src', relativePath);
      const pathSegments = relativeToSrc.split(path.sep);
      const lastItem = pathSegments.pop();
      const fileName = lastItem !== 'index' ? lastItem : pathSegments.pop();
      const dir = path.join(targetDir, ...pathSegments);
      const outputFile = `${fileName}.js`;
      const outputFilePath = path.resolve(__dirname, dir, outputFile);
      const hyphenatedName = pathSegments
        .slice(isMain ? 0 : 1)
        .concat(fileName)
        .join('-');

      return {
        config: {
          input: { [fileName]: relativePath },
          output: { dir, format: isMain ? 'es' : 'iife' },
        },
        isMain,
        metaPath: this.getMetaPath(path.dirname(file)),
        outputFile,
        outputFilePath,
        hyphenatedName,
      };
    });
  };

  createUserDataPlugin = () => {
    const { userDataMap } = this;
    return {
      name: 'create-user-data-plugin',
      banner() {
        const data = Array.from(userDataMap.values()).map(
          ({ runAt, matchRules, includeRules, excludeRules, outputFilePath }) => ({
            runAt,
            matchRules,
            includeRules,
            excludeRules,
            fileContent: fs.readFileSync(outputFilePath, 'utf8'),
          }),
        );
        return `\nconst userDatas = ${JSON.stringify(data)};\n`;
      },
    };
  };

  generateScriptHeaderPlugin = ({ metaPath, outputFile }, transformHeaderContent) => {
    return userScriptHeader({
      metaPath,
      transformHeaderContent(items) {
        const newItem = items
          .filter(([name]) => !['@supportURL', '@updateURL', '@downloadURL'].includes(name))
          .map(([name, value]) => [name, isDev && name === '@name' ? `${value} Dev` : value]);
        return transformHeaderContent(newItem);
      },
      outputFile,
    });
  };

  scriptHeaderPlugin = ({ metaPath, outputFile, outputFilePath, hyphenatedName }) => {
    const { userDataMap, userGrantsMap } = this;
    return this.generateScriptHeaderPlugin({ metaPath, outputFile }, (items) => {
      const data = {
        runAt: 'document-end',
        matchRules: [],
        includeRules: [],
        excludeRules: [],
        outputFilePath,
      };
      const grants = [];
      const newItems = items.map(([name, value]) => {
        if (name === '@run-at') data.runAt = value;
        if (name === '@match') data.matchRules.push(value);
        if (name === '@include') data.includeRules.push(value);
        if (name === '@exclude') data.excludeRules.push(value);
        if (name === '@grant') grants.push(value);
        return [name, name === '@name' ? value.replace(pkg.name, hyphenatedName) : value];
      });
      userDataMap.set(outputFilePath, data);
      userGrantsMap.set(outputFilePath, grants);
      return newItems;
    });
  };

  mainScriptHeaderPlugin = ({ metaPath, outputFile }) => {
    const { userGrantsMap, userScriptHeaderContent } = this;
    return this.generateScriptHeaderPlugin({ metaPath, outputFile }, (items) => {
      const newItems = items.concat(
        Array.from(new Set(Array.from(userGrantsMap.values()).flat()), (item) => ['@grant', item]),
      );
      userScriptHeaderContent.splice(0, userScriptHeaderContent.length, ...newItems);
      return newItems;
    });
  };

  devEntryPlugin = (outputFileName) => {
    const { userDataMap, userScriptHeaderContent } = this;
    let headerPluginApi;
    let devFileContentCache = '';
    return {
      name: 'generate-dev-entry',
      buildStart(options) {
        userDataMap.forEach(({ outputFilePath }) => {
          this.addWatchFile(outputFilePath);
        });
        const { plugins } = options;
        const pluginName = 'tampermonkey-header';
        const headerPlugin = plugins.find(({ name }) => name === pluginName);
        if (!headerPlugin) {
          // or handle this silently if it is optional
          throw new Error(`This plugin depends on the "${pluginName}" plugin.`);
        }
        // now you can access the API methods in subsequent hooks
        headerPluginApi = headerPlugin.api;
      },
      generateBundle(options) {
        const { dir } = options;
        const filePath = path.resolve(__dirname, dir, outputFileName);
        userScriptHeaderContent.push(['@require', 'file://' + filePath]);
        const devFileName = 'dev.user.js';
        const devFilePath = path.resolve(__dirname, dir, devFileName);
        const devFileContent =
          headerPluginApi?.generateUserScriptHeader(userScriptHeaderContent) ?? '';

        if (devFileContentCache !== devFileContent) {
          this.emitFile({
            type: 'asset',
            fileName: devFileName,
            source: devFileContent,
          });

          if (!devFileContentCache) {
            console.log(
              '\n✅Dev plugin is created. Please paste the path to browser and install in Tampermonkey: \n\x1b[1m\x1b[4m\x1b[36m%s\x1b[0m\n',
              devFilePath,
            );
          } else {
            console.log(
              '\n🔥Dev plugin need re-install. Please paste the path to browser and reinstall in Tampermonkey: \n\x1b[1m\x1b[4m\x1b[36m%s\x1b[0m\n',
              devFilePath,
            );
          }

          devFileContentCache = devFileContent;
        }
      },
    };
  };

  entryPlugins = ({ isMain, metaPath, outputFile, outputFilePath, hyphenatedName }) => {
    const plugins = isMain
      ? [
          this.mainScriptHeaderPlugin({ metaPath, outputFile, outputFilePath }),
          this.createUserDataPlugin(),
        ]
      : [this.scriptHeaderPlugin({ metaPath, outputFile, outputFilePath, hyphenatedName })];

    if (isMain && isDev) plugins.push(this.devEntryPlugin(outputFile));
    return plugins;
  };
}

const { getScriptConfig, entryPlugins } = new CustomTampermonkeyPlugin();

const callbackfn = ({ config, isMain, metaPath, outputFile, outputFilePath, hyphenatedName }) => ({
  ...config,
  plugins: [
    ...commonConfigs.plugins,
    ...entryPlugins({ isMain, metaPath, outputFile, outputFilePath, hyphenatedName }),
  ],
});

const devConfigs = () => {
  return defineConfig(
    getScriptConfig('dist').map((config) => ({
      ...callbackfn(config),
      watch: { exclude: 'dist' },
    })),
  );
};

const prodConfigs = () => {
  return defineConfig(getScriptConfig('build').map(callbackfn));
};

export default isDev ? devConfigs() : prodConfigs();
